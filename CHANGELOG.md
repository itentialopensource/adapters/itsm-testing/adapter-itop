
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:08PM

See merge request itentialopensource/adapters/adapter-itop!14

---

## 0.4.3 [08-28-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-itop!12

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:16PM

See merge request itentialopensource/adapters/adapter-itop!11

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:30PM

See merge request itentialopensource/adapters/adapter-itop!10

---

## 0.4.0 [07-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-itop!9

---

## 0.3.4 [03-27-2024]

* Changes made at 2024.03.27_14:00PM

See merge request itentialopensource/adapters/itsm-testing/adapter-itop!8

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_15:02PM

See merge request itentialopensource/adapters/itsm-testing/adapter-itop!7

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:44PM

See merge request itentialopensource/adapters/itsm-testing/adapter-itop!6

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:08AM

See merge request itentialopensource/adapters/itsm-testing/adapter-itop!5

---

## 0.3.0 [12-31-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-itop!4

---

## 0.2.2 [06-07-2022]

* Body in query stringifies but not in genericrequests

See merge request itentialopensource/adapters/itsm-testing/adapter-itop!3

---

## 0.2.1 [06-07-2022]

* Put Body into Query per what has been found in testing

See merge request itentialopensource/adapters/itsm-testing/adapter-itop!2

---

## 0.2.0 [05-25-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-itop!1

---

## 0.1.2 [03-28-2022] & 0.1.1 [03-28-2022]

- Initial Commit

See commit 905048f

---
