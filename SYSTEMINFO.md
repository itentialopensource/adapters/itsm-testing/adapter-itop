# iTop

Vendor: Combodo
Homepage: https://www.combodo.com/?lang=en

Product: iTop
Product Page: https://www.combodo.com/itop-193

## Introduction
We classify iTop into the ITSM (Service Management) domain as iTop provides ticketing solutions for Incident and Problem Management.

"iTop has been created to manage the complexity of shared infrastructures." 
"iTop is at the heart of operational activities for service centers." 

## Why Integrate
The iTop adapter from Itential is used to integrate the Itential Automation Platform (IAP) with iTop. With this adapter you have the ability to perform operations such as:

- Get Ticket
- Create Ticket
- Update Ticket

## Additional Product Documentation
The [API documents for iTop](https://www.itophub.io/wiki/page?id=latest:advancedtopics:rest_json)
The [Authentication for iTop](https://www.postman.com/combodo/workspace/combodo-s-public-workspace/documentation/13643004-60a55a4f-73ba-431a-9889-a319b0badadf)
