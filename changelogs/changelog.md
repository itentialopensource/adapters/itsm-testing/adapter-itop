
## 0.2.2 [06-07-2022]

* Body in query stringifies but not in genericrequests

See merge request itentialopensource/adapters/itsm-testing/adapter-itop!3

---

## 0.2.1 [06-07-2022]

* Put Body into Query per what has been found in testing

See merge request itentialopensource/adapters/itsm-testing/adapter-itop!2

---

## 0.2.0 [05-25-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-itop!1

---

## 0.1.2 [03-28-2022] & 0.1.1 [03-28-2022]

- Initial Commit

See commit 905048f

---
